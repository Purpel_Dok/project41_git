// Copyright Epic Games, Inc. All Rights Reserved.

#include "Project41_gitGameMode.h"
#include "Project41_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProject41_gitGameMode::AProject41_gitGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
