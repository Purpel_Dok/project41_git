// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project41_gitGameMode.generated.h"

UCLASS(minimalapi)
class AProject41_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProject41_gitGameMode();
};



